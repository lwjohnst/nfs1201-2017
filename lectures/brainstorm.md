
- nutr epi

- saturated fat vs health/disease
    - PURE
    - Meta (from McMaster)
    - The biology (long chain vs very long chain vs medium chain etc)

- statistical challenges
    - how to analyze the data? One by one, each food item? Dietary food indices
    (Mediterranean, Healthy Eating Index)?
    
- eg guidelines on veg and fruit are also based on evidence with health and
disease risk

- from Ray Carrol
    - Individual foods vs whole food pattern
    - Very complicated to collect, very complicated to analyze
    - eg 24hr recall, but can't do that too much
    - How does that translate to whole population?

- assume most people are meeting adequate nutrient intake
    - any additional intake is for health optimization
    - but... how do we measure that?
    - you know the challenges of measuring diet
    - what about the challenges of measuring disease? Especially chronic disease?
    - compounding that with measuring over long term
        - identifying cause and effect
        - extremely expensive to do large, long term, good quality RCT
            - challenge since it isn't blinded

- All forms of data are context-dependent "highest" forms of evidence
- Lorenzo's oil
    - very rare disease, case study
        - highest level of evidence that lead to an effective treatment
        - impossible to do any epidemiological or RCT study on this type of disease
        

