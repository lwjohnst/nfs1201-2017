

- on obesity

- how does obesity impact health?
    - evidence around obesity
    - confounding with socioeconomic status

-----

Potential overarching question:

- What role does nutrition play in obesity and subsequent health risk?
- What type of guidelines would we provide to reduce obesity and health risk?
- Is obesity itself at the population level a problem?
- Is obesity a health risk?
- Is obesity a nutrition problem?
- Is obesity adequately measured? (e.g. BMI)
- Is obesity a nutrition problem?
- How strong is the evidence for nutrition's role in obesity?
- 

Overall emphasis of assignment:

- Targetted to policymakers
- Assessment of the literature
- Development of an intervention
- Radical and drastic solution
- Start from stratch, new city, new planet, entirely new rules. 
    - What three things would you recommend changing for obesity?
        - Policymakers/city builders may choose all of them, or only one.
        - List in order of priority
    - Is nutrition one of them? Why?

Potential structure of submitted assignment:

- Evidence on their topic, all the limitations, all the strengths, is it feasible?
- Evidence, implementation, surveillance
- Evidence, arguments to convince policy makers

