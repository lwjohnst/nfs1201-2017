---
output: 
    word_document:
        reference_docx: "template.docx"
---

# NFS1201: Assignment 2 (Oct. 18th, 2017)

Obesity and higher fat mass (especially abdominal) is a risk factor for many
chronic diseases, including type 2 diabetes and cardiovascular disease. In
Canada, about 20%[^1] of adults are obese (based on BMI), with this number
rising. Globally, an estimated 14%[^2] of adults are obese, more than double the
rate since 1980. On top of this are the increasing number of children who are
obese. This is a major public health concern due to the higher risk for chronic
diseases, given how preventable obesity is. There are recent efforts to
introduce new policies and governmental regulations on managing rates of obesity
that target the diet. Specifically, there are calls to impose a tax on
sugar-sweetened beverages (SSB; e.g. Coke, Pepsi) from multiple sources and
organizations, including magazine opinion pieces[^3], the WHO[^4], Dietitians of
Canada[^5], and Diabetes Canada[^6]. There is much discussion about the potential
effectiveness of taxing SSB to reduce obesity. There are legitimate arguments
and evidence for and against using taxation on SSB as an effective means to
reduce obesity.

[^1]: Based on data from [Statistics Canada](http://www.statcan.gc.ca/pub/82-625-x/2015001/article/14185-eng.htm) from 2014 surveys of self-reported weight and height.
[^2]: Based on [WHO reports](http://www.who.int/mediacentre/factsheets/fs311/en/).
[^3]: Globe and Mail: https://beta.theglobeandmail.com/news/national/sugar-tax-debate-canada-soda-childhood-obesity/article36591805/?ref=http://www.theglobeandmail.com&
[^4]: WHO: http://www.who.int/mediacentre/news/releases/2016/curtail-sugary-drinks/en/
[^5]: Webpage on DC's statement: https://www.dietitians.ca/Dietitians-Views/Sugar-sweetened-Beverages-and-Taxation.aspx
[^6]: Diabetes Canada: https://www.diabetes.ca/newsroom/search-news/why-a-sugar-sweetened-beverage-tax-will-help

As it stands, it seems likely that this proposed taxation will be passed by the
government. However, like all legislation, there are always positive and/or
negative side-effects. What do you think are the possible unintended
consequences of this 'sugar tax'? What might be needed to manage these possible
effects at a population level? Be sure to consider potential economic,
corporate, health, and social factors (e.g. industy promotion of non-nutritive
sweeteners or other beverage products, change in consumer behaviour patterns,
influence of socioeconomic status), and whether these consequences are overall
beneficial or harmful and why that is.

Provide a clear statement of your overall stance at the start of your essay as
well as a strong conclusion that summarizes your evidence and arguments.
Regardless of your stance and perspectives, it is expected that you provide
evidence based on a critical evaluation of the literature and that your
arguments are rationally sound and logical. Your assignment should be
approximately 1,500 words in length, not including references. It should be
submitted to FitzGerald building room 315 by 5pm on Wednesday, Nov. 10th, 2017
(any extension to this deadline will require prior approval from the
instructor). Assignments should be submitted by hard-copy; emailing or faxing
are possible subject to instructor approval. **Important**: please include a
title page, with your name, and start the assignment on the *next* page (still
double-side the pages).
